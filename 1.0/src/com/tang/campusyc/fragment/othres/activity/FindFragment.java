package com.tang.campusyc.fragment.othres.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.GetCallback;

import com.tang.campusyc.R;
import com.tang.campusyc.data_claaback.Lost;
import com.tang.campusyc.toast.DiyToast;

public class FindFragment extends Fragment {
	private ListView lv_1;
	private ArrayAdapter<String> LostAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.activity_find_find, container,
				false);
		get_find_list_from_bmobsql();
		handler.post(timeRunnable);
		return view;
	}

	private void get_find_list_from_bmobsql() {
		// TODO Auto-generated method stub
		final BmobQuery<Lost> query = new BmobQuery<Lost>();
		// 按照时间降序
		query.order("-createdAt");
		// 执行查询，第一个参数为上下文，第二个参数为查找的回调
		query.findObjects(getActivity(), new FindListener<Lost>() {

			@Override
			public void onSuccess(List<Lost> losts) {
				// 将结果显示在列表中
				
			}

			@Override
			public void onError(int code, String arg0) {
				DiyToast.showToast(getActivity(), "出现错误：" + arg0);
			}
		});
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			get_find_list_from_bmobsql();
			handler.postDelayed(timeRunnable, 1000);
		}
	};
	Runnable timeRunnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			Message msg = handler.obtainMessage();
			handler.sendMessage(msg);
		}
	};

}
