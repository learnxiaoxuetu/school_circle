package com.tang.campusyc.fragment.othres.activity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import cn.bmob.v3.listener.SaveListener;

import com.tang.campusyc.R;
import com.tang.campusyc.data_claaback.Found;
import com.tang.campusyc.data_claaback.Lost;
import com.tang.campusyc.toast.DiyToast;

/**
 * @author Administrator
 * @year 2019
 * @Todo TODO 发布界面
 * @package_name com.tang.campusyc.fragment.othres.activity
 * @project_name キャンパス0.01Demo
 * @file_name HelpFragment.java
 */
public class HelpFragment extends Fragment implements OnClickListener {
	private EditText et_title_find;
	private EditText et_phone_find;
	private EditText et_miaoshu_find;
	private Button btn_find_con;
	private Button btn_find_resert;
	private String title, phone, miaoshu;
	private TextView tv_last_number;
	private int last_number = 200;
	private Spinner sp_get_lost;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.activity_find_help, container,
				false);
		initView(view);// 绑定
		edit_set_number();// 字数
		return view;
	}

	private void initView(View view) {
		// TODO Auto-generated method stub
		sp_get_lost = (Spinner) view.findViewById(R.id.sp_lost_find);
		btn_find_con = (Button) view.findViewById(R.id.btn_find_con);
		btn_find_resert = (Button) view.findViewById(R.id.btn_find_resert);
		et_miaoshu_find = (EditText) view.findViewById(R.id.et_miaoshu_find);
		et_phone_find = (EditText) view.findViewById(R.id.et_phone_find);
		et_title_find = (EditText) view.findViewById(R.id.et_title_find);
		btn_find_con.setOnClickListener(this);
		btn_find_resert.setOnClickListener(this);
		tv_last_number = (TextView) view.findViewById(R.id.tv_last_number);
		tv_last_number.setText("200");
	}

	private void edit_set_number() {
		et_miaoshu_find.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (start > 200) {
					DiyToast.showToast(getActivity(), "最高200字~");
					tv_last_number.setTextColor(Color.RED);
				} else {
					tv_last_number.setTextColor(Color.BLACK);
				}
				tv_last_number.setText(String
						.valueOf((last_number - 1) - start));
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
	}

	/**
	 * 手机号号段校验， 第1位：1； 第2位：{3、4、5、6、7、8}任意数字； 第3—11位：0—9任意数字
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isTelPhoneNumber(String value) {
		if (value != null && value.length() == 11) {
			Pattern pattern = Pattern.compile("^1[3|4|5|6|7|8][0-9]\\d{8}$");
			Matcher matcher = pattern.matcher(value);
			return matcher.matches();
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_find_con:
			title = et_title_find.getText().toString();
			phone = et_phone_find.getText().toString();
			miaoshu = et_miaoshu_find.getText().toString();
			if (title.isEmpty() || phone.isEmpty() || miaoshu.isEmpty()) {
				DiyToast.showToast(getActivity(), "不能有空白项，更详细的描述才能帮你找回你的东西哦！");
			} else {
				if (isTelPhoneNumber(phone)) {
					if (sp_get_lost.getSelectedItem().toString()
							.equals("我捡到了东西")) {
						new AlertDialog.Builder(getActivity())
								.setTitle("提示")
								.setMessage("你确定要发布“我捡到了东西”吗？")
								.setPositiveButton("确定",
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												// TODO Auto-generated method
												// stub
												Found found = new Found();
												found.setDescribe(miaoshu);
												found.setPhone(phone);
												found.setTitle(title);
												found.save(getActivity(),
														new SaveListener() {

															@Override
															public void onSuccess() {
																// TODO
																// Auto-generated
																// method stub
																DiyToast.showToast(
																		getActivity(),
																		"发布成功");
																et_miaoshu_find
																		.setText(null);
																et_phone_find
																		.setText(null);
																et_title_find
																		.setText(null);
															}

															@Override
															public void onFailure(
																	int code,
																	String arg0) {
																// TODO
																// Auto-generated
																// method stub
																DiyToast.showToast(
																		getActivity(),
																		"失败："
																				+ arg0);
															}
														});
											}
										}).setNegativeButton("取消", null).show();
					}
					if (sp_get_lost.getSelectedItem().toString()
							.equals("我丢掉了东西")) {
						new AlertDialog.Builder(getActivity())
								.setTitle("提示")
								.setMessage("你确定要发布“我丢掉了东西”吗？")
								.setPositiveButton("确定",
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												// TODO Auto-generated method
												// stub
												Lost lost = new Lost();
												lost.setDescribe(miaoshu);
												lost.setPhone(phone);
												lost.setTitle(title);
												lost.save(getActivity(),
														new SaveListener() {

															@Override
															public void onSuccess() {
																// 其他代码
																DiyToast.showToast(
																		getActivity(),
																		"发布成功");
																et_miaoshu_find
																		.setText(null);
																et_phone_find
																		.setText(null);
																et_title_find
																		.setText(null);
															}

															@Override
															public void onFailure(
																	int code,
																	String arg0) {
																DiyToast.showToast(
																		getActivity(),
																		"失败："
																				+ arg0);
															}
														});
											}
										})
								.setNegativeButton("取消",
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												// TODO Auto-generated method
												// stub

											}
										}).show();

					}
				} else {
					DiyToast.showToast(getActivity(), "您的电话号码似乎错误");
				}
			}
			break;
		case R.id.btn_find_resert:
			et_miaoshu_find.setText(null);
			et_phone_find.setText(null);
			et_title_find.setText(null);
			break;
		default:
			break;
		}
	}
}