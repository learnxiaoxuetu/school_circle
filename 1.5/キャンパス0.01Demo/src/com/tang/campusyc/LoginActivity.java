package com.tang.campusyc;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.listener.SaveListener;

import com.tang.campusyc.api.BmobSystemAPI;
import com.tang.campusyc.fragment.IndexActivity;
import com.tang.campusyc.textchanger.TextChanger;
import com.tang.campusyc.toast.DiyToast;

public class LoginActivity extends Activity {
	private Button btn_login, btn_reg, btn_exit;
	private EditText et_user;
	private EditText et_pass;
	private String user, pass;
	private TextView tv_reback_pass;
	private CheckBox cb_autologin;
	public static SharedPreferences sharedPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 顶栏
		setContentView(R.layout.activity_login);
		initView();// 绑定控件
		// 初始化Bmob API
		Bmob.initialize(getApplicationContext(), BmobSystemAPI.apiString);
		// 设置点击记住密码时提示
		cb_autologin.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							LoginActivity.this);
					builder.setTitle("提示");
					builder.setMessage("确定要记住密码吗？");
					builder.setPositiveButton("确定",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									cb_autologin.setChecked(true);
									sharedPreferences
											.edit()
											.putString(
													"now_user",
													et_user.getText()
															.toString())
											.putBoolean("rember", true)
											.commit();
								}
							});
					builder.setNegativeButton("取消",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									cb_autologin.setChecked(false);
									sharedPreferences.edit()
											.putBoolean("rember", false)
											.commit();
								}
							});
					builder.show();
				} else {
					cb_autologin.setChecked(false);
					sharedPreferences.edit().putBoolean("rember", false)
							.commit();
				}
			}
		});
		// 找回密码
		tv_reback_pass.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(getApplicationContext(),
						ReBackPassActivity.class));
			}
		});
		// 退出按钮
		btn_exit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
		// 注册按钮
		btn_reg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(getApplicationContext(),
						RegActivity.class));
			}
		});
		// 登录按钮
		btn_login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				user = et_user.getText().toString();
				pass = et_pass.getText().toString();
				// 新建加载Dialog
				final ProgressDialog progressDialog = new ProgressDialog(
						LoginActivity.this);
				progressDialog.setMessage("正在登陆......");
				progressDialog.setCancelable(false);
				progressDialog.setIcon(android.R.drawable.ic_dialog_info);
				progressDialog.show();
				// 判断条件
				if (user.isEmpty()) {
					DiyToast.showToast(getApplicationContext(), "请输入用户名");
					progressDialog.dismiss();
				} else if (pass.isEmpty()) {
					DiyToast.showToast(getApplicationContext(), "请输入密码");
					progressDialog.dismiss();
				} else {
					sharedPreferences.edit().putString("user", user)
							.putString("now_user", user)
							.putString("pass", pass).commit();
					BmobUser user = new BmobUser();
					user.setUsername(LoginActivity.this.user);
					user.setPassword(pass);
					user.login(getApplicationContext(), new SaveListener() {

						@Override
						public void onSuccess() {
							// TODO Auto-generated method stub
							progressDialog.dismiss();
							DiyToast.showToast(getApplicationContext(),
									"连接Bmob数据库登陆完成");
							startActivity(new Intent(getApplicationContext(),
									IndexActivity.class));
							finish();
						}

						@Override
						public void onFailure(int arg0, String arg1) {
							// TODO Auto-generated method stub
							DiyToast.showToast(getApplicationContext(), "失败："
									+ arg1);
							progressDialog.dismiss();
						}
					});
				}
			}
		});
		// 设置文本框内容样式
		et_pass.setTransformationMethod(new TextChanger());
	};

	/*
	 * 绑定控件
	 */
	private void initView() {
		// TODO Auto-generated method stub
		cb_autologin = (CheckBox) findViewById(R.id.cb_autologin);
		tv_reback_pass = (TextView) findViewById(R.id.tv_reback_pass);
		btn_exit = (Button) findViewById(R.id.btn_exit);
		btn_login = (Button) findViewById(R.id.btn_login);
		btn_reg = (Button) findViewById(R.id.btn_reg);
		et_pass = (EditText) findViewById(R.id.et_passward);
		et_user = (EditText) findViewById(R.id.et_username);
		sharedPreferences = getSharedPreferences("rember", MODE_WORLD_WRITEABLE);
		et_user.setText(sharedPreferences.getString("user", null));
		et_pass.setText(sharedPreferences.getString("pass", null));
	}
}