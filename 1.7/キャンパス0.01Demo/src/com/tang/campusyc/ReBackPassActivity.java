package com.tang.campusyc;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.GetListener;
import cn.bmob.v3.listener.UpdateListener;

import com.tang.campusyc.toast.DiyToast;

public class ReBackPassActivity extends Activity {
	private Button btn_reback_pass;
	private EditText et_reback_user, et_reback_id, et_new_pass;
	private String user, id, new_pass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_re_back_pass);
		initView();
		btn_reback_pass.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new_pass = et_new_pass.getText().toString();
				id = et_reback_id.getText().toString();
				user = et_reback_user.getText().toString();
				final ProgressDialog progressDialog = new ProgressDialog(
						ReBackPassActivity.this);
				progressDialog.setMessage("正在查找......");
				progressDialog.setCancelable(false);
				progressDialog.setIcon(android.R.drawable.ic_dialog_info);
				progressDialog.show();
				if (id.isEmpty() || user.isEmpty()) {
					DiyToast.showToast(getApplicationContext(), "不能有空白项");
					progressDialog.dismiss();
				} else {
					BmobQuery<BmobUser> bmobQuery = new BmobQuery<BmobUser>();
					bmobQuery.getObject(ReBackPassActivity.this, id,
							new GetListener<BmobUser>() {

								@Override
								public void onFailure(int arg0, String arg1) {
									// TODO Auto-generated method stub
									DiyToast.showToast(getApplicationContext(),
											"错误" + arg1);
									progressDialog.dismiss();
								}

								@Override
								public void onSuccess(BmobUser arg0) {
									// TODO Auto-generated method stub
									if (user.equals(arg0.getUsername())) {
										final BmobUser bmobUser = new BmobUser();
										bmobUser.setPassword(new_pass);
										bmobUser.update(
												ReBackPassActivity.this, id,
												new UpdateListener() {

													@Override
													public void onFailure(
															int arg0,
															String arg1) {
														// TODO Auto-generated
														// method stub
														progressDialog
																.dismiss();
														DiyToast.showToast(
																getApplicationContext(),
																"失败：" + arg1);
													}

													@Override
													public void onSuccess() {
														// TODO Auto-generated
														// method stub
														DiyToast.showToast(
																getApplicationContext(),
																"修改成功，新密码："
																		+ new_pass);
														progressDialog
																.dismiss();
														finish();
													}
												});
									} else {
										DiyToast.showToast(
												getApplicationContext(),
												"输入的用户似乎不存在");
										progressDialog.dismiss();
									}
								}
							});
				}
			}
		});
	}

	private void initView() {
		// TODO Auto-generated method stub
		btn_reback_pass = (Button) findViewById(R.id.btn_reback_pass);
		et_reback_id = (EditText) findViewById(R.id.et_reback_id);
		et_reback_user = (EditText) findViewById(R.id.et_reback_user);
		et_new_pass = (EditText) findViewById(R.id.et_new_pass);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_re_back_pass, menu);
		return true;
	}

}
