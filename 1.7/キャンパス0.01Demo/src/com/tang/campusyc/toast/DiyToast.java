package com.tang.campusyc.toast;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.tang.campusyc.R;

/**
 * @author Administrator
 * @year 2019
 * @Todo TODO �Զ���Toast
 * @package_name com.tang.campusyc.toast
 * @project_name �����ѥ�0.01Demo
 * @file_name DiyToast.java
 */
public class DiyToast {
	public static Toast toast;

	public static void showToast(Context context, String string) {
		if (toast == null) {
			toast = Toast.makeText(context, string, Toast.LENGTH_SHORT);
		} else {
			toast.setText(string);
		}
		toast.show();
	}
}
