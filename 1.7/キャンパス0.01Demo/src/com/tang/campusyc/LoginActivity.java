package com.tang.campusyc;

import android.Manifest;
import android.Manifest.permission;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.listener.SaveListener;

import com.tang.campusyc.api.BmobSystemAPI;
import com.tang.campusyc.fragment.views.IndexActivity;
import com.tang.campusyc.textchanger.TextChanger;
import com.tang.campusyc.toast.DiyToast;

public class LoginActivity extends Activity {
	private Button btn_login, btn_reg, btn_exit;
	private EditText et_user;
	private EditText et_pass;
	private String user, pass;
	private TextView tv_reback_pass;
	private CheckBox cb_autologin;
	public static SharedPreferences sharedPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 顶栏
		setContentView(R.layout.activity_login);
		initView();// 绑定控件
		// 初始化Bmob API
		Bmob.initialize(getApplicationContext(), BmobSystemAPI.apiString);
		// 首次开启打开提示框
		if (sharedPreferences.getBoolean("permission", false) == true) {
		} else {
			get_version();
		}
		// 设置点击记住密码时提示
		cb_autologin.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							LoginActivity.this);
					builder.setTitle("提示");
					builder.setMessage("确定要记住密码吗？");
					builder.setPositiveButton("确定",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									cb_autologin.setChecked(true);
									sharedPreferences
											.edit()
											.putString(
													"now_user",
													et_user.getText()
															.toString())
											.putBoolean("rember", true)
											.commit();
								}
							});
					builder.setNegativeButton("取消",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									cb_autologin.setChecked(false);
									sharedPreferences.edit()
											.putBoolean("rember", false)
											.commit();
								}
							});
					builder.show();
				} else {
					cb_autologin.setChecked(false);
					sharedPreferences.edit().putBoolean("rember", false)
							.commit();
				}
			}
		});
		// 找回密码
		tv_reback_pass.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(getApplicationContext(),
						ReBackPassActivity.class));
			}
		});
		// 退出按钮
		btn_exit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
		// 注册按钮
		btn_reg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(getApplicationContext(),
						RegActivity.class));
			}
		});
		// 登录按钮
		btn_login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				user = et_user.getText().toString();
				pass = et_pass.getText().toString();
				// 新建加载Dialog
				final ProgressDialog progressDialog = new ProgressDialog(
						LoginActivity.this);
				progressDialog.setMessage("正在登陆......");
				progressDialog.setCancelable(false);
				progressDialog.setIcon(android.R.drawable.ic_dialog_info);
				progressDialog.show();
				// 判断条件
				if (user.isEmpty()) {
					DiyToast.showToast(getApplicationContext(), "请输入用户名");
					progressDialog.dismiss();
				} else if (pass.isEmpty()) {
					DiyToast.showToast(getApplicationContext(), "请输入密码");
					progressDialog.dismiss();
				} else {
					sharedPreferences.edit().putString("user", user)
							.putString("now_user", user)
							.putString("pass", pass).commit();
					BmobUser user = new BmobUser();
					user.setUsername(LoginActivity.this.user);
					user.setPassword(pass);
					user.login(getApplicationContext(), new SaveListener() {

						@Override
						public void onSuccess() {
							// TODO Auto-generated method stub
							progressDialog.dismiss();
							DiyToast.showToast(getApplicationContext(),
									"连接Bmob数据库登陆完成");
							startActivity(new Intent(getApplicationContext(),
									IndexActivity.class));
							finish();
						}

						@Override
						public void onFailure(int arg0, String arg1) {
							// TODO Auto-generated method stub
							DiyToast.showToast(getApplicationContext(), "失败："
									+ arg1);
							progressDialog.dismiss();
						}
					});
				}
			}
		});
		// 设置文本框内容样式
		et_pass.setTransformationMethod(new TextChanger());
	};

	// 获取手机android版本
	private void get_version() {
		if (Build.VERSION.SDK_INT >= 23) {
			// 此处做动态权限申请
			System.out.println("版本：" + Build.VERSION.SDK_INT + "，高于23,需要动态权限");
			show_dialog_permission_get();// 展示对话框，告诉用户需要的权限
		} else {
			// 低于23 不需要特殊处理
			System.out.println("版本：" + Build.VERSION.SDK_INT + "，低于23,无需动态权限");
		}
	}

	// 检查权限申请状态
	private void show_dialog_permission_get() {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				LoginActivity.this);
		View view = LayoutInflater.from(LoginActivity.this).inflate(
				R.layout.dialog_permission_get, null, false);
		builder.setView(view);
		builder.setMessage("由于您是第一次打开本程序，" + "\n"
				+ "并且您的SDK版本高于23，即安卓版本大于6.0，版本号23以下申请权限无需用户同意，但是现在需要了" + "\n"
				+ "简单来说，本程序需要一些权限经过用户同意后才能正常使用。" + "\n" + "本提示框仅作为提醒用途，" + "\n"
				+ "本程序不会使用大量涉及隐私的权限，" + "\n" + "展示出的已经是所有权限，并且每一个权限下有详细的介绍"
				+ "\n" + "请用户酌情给予。" + "\n" + "最后更新于2019/11/25");
		builder.setTitle("提示");
		builder.setPositiveButton("不再提示",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						sharedPreferences.edit().putBoolean("permission", true)
								.commit();
					}
				});
		builder.setNeutralButton("下次提示", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

			}
		});
		builder.show();
	}

	/*
	 * 绑定控件
	 */
	private void initView() {
		// TODO Auto-generated method stub
		cb_autologin = (CheckBox) findViewById(R.id.cb_autologin);
		tv_reback_pass = (TextView) findViewById(R.id.tv_reback_pass);
		btn_exit = (Button) findViewById(R.id.btn_exit);
		btn_login = (Button) findViewById(R.id.btn_login);
		btn_reg = (Button) findViewById(R.id.btn_reg);
		et_pass = (EditText) findViewById(R.id.et_passward);
		et_user = (EditText) findViewById(R.id.et_username);
		sharedPreferences = getSharedPreferences("rember", MODE_WORLD_WRITEABLE);
		et_user.setText(sharedPreferences.getString("user", null));
		et_pass.setText(sharedPreferences.getString("pass", null));
	}
}