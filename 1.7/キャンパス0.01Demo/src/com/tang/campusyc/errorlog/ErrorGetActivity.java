package com.tang.campusyc.errorlog;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.webkit.WebView;

import com.tang.campusyc.R;

public class ErrorGetActivity extends Activity {
	public static String errorlog_get = "";
	private String SKEY = "SCU66788Tac2bf7385575174e067c917d471e25365dd3983cec5ee";
	private String web_index = "sc.ftqq.com";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_error_get);
		test(errorlog_get);
	}

	private void test(String errorlog) {
		WebView wv = (WebView) findViewById(R.id.web_error_log);
		wv.loadUrl("https://" + web_index + "/" + SKEY
				+ ".send?text=【APP】收到一条日志 " + "-------" + errorlog);
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_error_get, menu);
		return true;
	}

}
