package com.tang.campusyc.fragment;

import java.util.Calendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tang.campusyc.Classes_Activity;
import com.tang.campusyc.R;
import com.tang.campusyc.toast.DiyToast;

public class IndexFragment extends Fragment {
	private Button btn_class_table;
	private TextView tv_count_time_for_jiaqi;
	private TextView tv_now_day;
	private ImageView img_date;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_index, container, false);
		initView(view);// 绑定控件
		// check_now_week_for_fangjia();// 获取当前周数
		// check_now_day_for_fangjia();// 获取当前日期
		setTime();
		// 显示课程表
		btn_class_table.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub\
				startActivity(new Intent(getActivity(), Classes_Activity.class));
			}
		});
		// 头像图片点击
		img_date.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
		return view;
	}

	/**
	 * 设置校历中日期的时间
	 */
	public void setTime() {
		Calendar calendar = Calendar.getInstance();
		String year = calendar.get(Calendar.YEAR) + "";
		String month = calendar.get(Calendar.MONTH) + 1 + "";
		String day = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String week = calendar.get(Calendar.WEEK_OF_YEAR) + "";
		String dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) + "";
		tv_count_time_for_jiaqi.setText(year + "年 第 " + week + " 周 " + " "
				+ " 星期 " + dayOfWeek);
		tv_now_day.setText(month + "  月  " + day + "  日  ");
	}

	private void initView(View view) {
		// TODO Auto-generated method stub
		tv_now_day = (TextView) view.findViewById(R.id.tv_now_day);
		img_date = (ImageView) view.findViewById(R.id.img_date);
		btn_class_table = (Button) view.findViewById(R.id.btn_class_table);
		tv_count_time_for_jiaqi = (TextView) view
				.findViewById(R.id.tv_count_time_for_jiaqi);
	}
}