package com.tang.campusyc.fragment.views;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;

import com.tang.campusyc.R;
import com.tang.campusyc.fragment.FindFragment;
import com.tang.campusyc.fragment.FoundFragment;
import com.tang.campusyc.fragment.HelpFragment;
import com.tang.campusyc.toast.DiyToast;

public class FindAndLostActivity extends FragmentActivity {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;
	private List<Fragment> faFragments = new ArrayList<Fragment>();
	private RadioButton ra_find;
	private RadioButton ra_lost;
	private RadioButton ra_help;
	private RadioButton[] radioButtons = new RadioButton[3];
	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_find_and_lost);
		add_fragment();// 添加界面
		initView();// 绑定控件
		// 刷新按钮
		findViewById(R.id.btn_resert).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				FoundFragment
						.get_find_list_from_bmobsql(getApplicationContext());
				FindFragment
						.get_find_list_from_bmobsql(getApplicationContext());
				DiyToast.showToast(getApplicationContext(), "更新成功");
			}
		});
		ra_find.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mViewPager.setCurrentItem(0);
			}
		});
		ra_help.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mViewPager.setCurrentItem(2);
			}
		});
		ra_lost.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mViewPager.setCurrentItem(1);
			}
		});
		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());
		// Set up the ViewPager with the sections adapter.
		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setOffscreenPageLimit(5);
		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						// actionBar.setSelectedNavigationItem(position);
						radioButtons[position].setChecked(true);
						switch (position) {
						case 0:
							findViewById(R.id.btn_resert).setVisibility(
									View.VISIBLE);
							break;
						case 1:
							findViewById(R.id.btn_resert).setVisibility(
									View.VISIBLE);
							break;
						case 2:
							findViewById(R.id.btn_resert).setVisibility(
									View.INVISIBLE);
							break;

						default:
							break;
						}
					}
				});
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		List<Fragment> fragments = new ArrayList<Fragment>();

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
			this.fragments = faFragments;
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return fragments.size();
		}

		@Override
		public Fragment getItem(int arg0) {
			// TODO Auto-generated method stub
			return fragments.get(arg0);
		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		mViewPager = (ViewPager) findViewById(R.id.pager);
		ra_find = (RadioButton) findViewById(R.id.ra_find);
		ra_help = (RadioButton) findViewById(R.id.ra_help);
		ra_lost = (RadioButton) findViewById(R.id.ra_lost);
		radioButtons[0] = ra_find;
		radioButtons[1] = ra_lost;
		radioButtons[2] = ra_help;
	}

	private void add_fragment() {
		// TODO Auto-generated method stub
		faFragments.add(new FoundFragment());
		faFragments.add(new FindFragment());
		faFragments.add(new HelpFragment());
	}
}
