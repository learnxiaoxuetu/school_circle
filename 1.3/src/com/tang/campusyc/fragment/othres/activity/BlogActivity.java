package com.tang.campusyc.fragment.othres.activity;

import com.tang.campusyc.R;
import com.tang.campusyc.R.layout;
import com.tang.campusyc.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.view.Menu;
import android.webkit.WebView;

public class BlogActivity extends Activity {
	private WebView my_web_view;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_blog);
		ProgressDialog progress_dialog = new ProgressDialog(BlogActivity.this);
		progress_dialog.setMessage("加载较慢，请稍后（这个提示框可以关闭）......");
		progress_dialog.show();
		my_web_view = (WebView) findViewById(R.id.my_web_view);
		my_web_view.addJavascriptInterface(new BlogActivity(), "AndroidNative");
		my_web_view.loadUrl("https://naiyouhuameitang.club");
	}
}
