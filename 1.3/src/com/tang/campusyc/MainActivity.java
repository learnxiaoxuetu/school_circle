package com.tang.campusyc;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.tang.campusyc.fragment.IndexActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * @author Administrator
 * @year 2019
 * @Todo TODO 主界面
 * @package_name com.tang.campusyc
 * @project_name キャンパス0.01Demo
 * @file_name MainActivity.java
 */
public class MainActivity extends Activity {
	private TextView tv_down_time;
	private TextView tv_tips;
	private CountDownTimer countDownTimer;
	private List<String> list = new ArrayList<String>();
	private Random random = new Random();
	private LinearLayout login_line_pass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initView();// 绑定
		add_list();// 添加句子
		start_yiyan();// 开屏一言（本地随机）
		// 倒计时
		countDownTimer = new CountDownTimer(5000, 1000) {

			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				tv_down_time.setText(String
						.valueOf(millisUntilFinished / 1000 % 60) + "秒后进入");
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				tv_down_time.setText("0sec");
				pass_opening();
			}
		}.start();
		// 点击左上角的布局进行跳过
		login_line_pass.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pass_opening();
			}
		});
		// 点击跳过
		tv_down_time.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pass_opening();
			}
		});

	}

	private void pass_opening() {
		// TODO Auto-generated method stub
		LoginActivity.sharedPreferences = getSharedPreferences("rember",
				MODE_WORLD_WRITEABLE);
		if (LoginActivity.sharedPreferences.getBoolean("rember", false) == true) {
			if (countDownTimer != null) {
				countDownTimer.cancel();
			}
			tv_down_time.setText("0sec");
			startActivity(new Intent(getApplicationContext(),
					IndexActivity.class));
			finish();
		} else {
			if (countDownTimer != null) {
				countDownTimer.cancel();
			}
			tv_down_time.setText("0sec");
			startActivity(new Intent(getApplicationContext(),
					LoginActivity.class));
			finish();
		}
	}

	private void add_list() {
		// TODO Auto-generated method stub
		list.add("应该努力地去生活，" + "\n" + "对过往闭口不提，" + "\n" + "是好是坏皆为经历。");
		list.add("以后，只对两种人好，" + "\n" + "一种是对我好的人，" + "\n" + "一种是懂得我的好的人。"
				+ "\n" + "在这短暂的生命里，" + "\n" + "一个人的温暖也是有限的啊，" + "\n"
				+ "一点都不能浪费。");
		list.add("人生很多滋味都要到一个年纪才懂得去细细品味，" + "\n" + "比如类似这种相濡以沫的感动和幸福。" + "\n"
				+ "然而当你一旦懂了，" + "\n" + "一切却都已经远了。");
		list.add("有的人对你好，" + "\n" + "是因为你对他好；" + "\n" + "有的人对你好，" + "\n"
				+ "是因为懂得你的好。");
		list.add("没有依托的条件，" + "\n" + "找不出凭借的资本，" + "\n" + "有的只是坚定的胸怀，" + "\n"
				+ "努力的心态。" + "\n" + "就算惆怅，" + "\n" + "偶有悲伤，" + "\n" + "也得一样继续，"
				+ "\n" + "人生就是带着生命所有的希望，" + "\n" + "用尽心思一直继续。");
		list.add("八月长江万里晴，千帆一道带风轻");
		list.add("红蓼渡头秋正雨，印沙鸥迹自成行，整鬟飘袖野风香");
		list.add("那天放学的时候人很多。" + "\n" + "楼道很挤，你的手不小心碰到了我的手背，" + "\n"
				+ "这是我漫长而又短暂的初恋。");
		list.add("我想和你见面，地点你选。" + "\n" + "森林，沙漠，夜晚依稀的湖畔。" + "\n"
				+ "草原，大海，清晨薄雾的街口。" + "\n" + "只是，别在梦里。");
		list.add("我喜欢你");
		list.add("燃烧吧，我的小宇宙！");
		list.add("如果我真的存在，也是因为你需要我。");
		list.add("固执守旧说的就是我了吧");
		list.add("天子坛中寻一笑，枕月云深魏无羡。");
		list.add("就算我们知道了彼此总有一天会分别，" + "\n" + "现在的时光，也绝不是没有意义的。");
		list.add("她有心，她的心在我这里。");
		list.add("谎言不一定是谎言，" + "\n" + "被发现的谎言，才算是谎言。");
		list.add("青春就像卫生纸。" + "\n" + "看着挺多的，用着用着就不够了。");
		list.add("科学就是去确定未知的事物");
		list.add("遇不可说之事，必须保持沉默。");
		list.add("你身上散发出的迷人气息， " + "\n" + "足以说明你是一朵艳丽的玫瑰 " + "\n" + "不过也是一场祸。");
		list.add("舞台上演员不能无视剧本随便演， " + "\n" + "华丽地退场才是完成使命。");
		list.add("容忍别人的一切错误");
		list.add("既然我无法停留， " + "\n" + "那么就飞到我再也不能飞的那一天吧。");
		list.add("斜晖脉脉水悠悠, " + "\n" + "肠断白频洲.");
		list.add("请不要跟我搭话。" + "\n" + "我讨厌你！");
	}

	/*
	 * 绑定控件
	 */
	private void initView() {
		// TODO Auto-generated method stub
		tv_down_time = (TextView) findViewById(R.id.tv_down_time);
		tv_tips = (TextView) findViewById(R.id.tv_tips);
		login_line_pass = (LinearLayout) findViewById(R.id.login_line_pass);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	private void start_yiyan() {
		// TODO Auto-generated method stub
		String string = list.get(random.nextInt(list.size())).toString();
		if (string.isEmpty()) {
			start_yiyan();
		} else {
			tv_tips.setText(string);// 设置文本
		}
	}

}
