package com.tang.campusyc.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tang.campusyc.LoginActivity;
import com.tang.campusyc.R;
import com.tang.campusyc.toast.DiyToast;

public class SettingActivity extends Activity {
	private LinearLayout line_setting_exit;
	private TextView tv_now_login_username;
	private int number = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_setting);
		initView();
		LoginActivity.sharedPreferences = getSharedPreferences("rember",
				MODE_WORLD_WRITEABLE);
		tv_now_login_username.setText(LoginActivity.sharedPreferences
				.getString("now_user", null));
		line_setting_exit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DiyToast.showToast(getApplicationContext(),
						"暂时不可用，如果想退出、切换账号，请清除本APP的数据");
			}
		});

	}

	private void initView() {
		// TODO Auto-generated method stub
		tv_now_login_username = (TextView) findViewById(R.id.tv_now_login_username);
		line_setting_exit = (LinearLayout) findViewById(R.id.line_setting_exit);
	}

}
