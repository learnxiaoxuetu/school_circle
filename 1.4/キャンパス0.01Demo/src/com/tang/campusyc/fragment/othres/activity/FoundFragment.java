package com.tang.campusyc.fragment.othres.activity;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;

import com.tang.campusyc.R;
import com.tang.campusyc.adapter.BaseAdapterHelper;
import com.tang.campusyc.adapter.QuickAdapter;
import com.tang.campusyc.data_claaback.Found;
import com.tang.campusyc.data_claaback.Lost;

public class FoundFragment extends Fragment {
	private ListView lv_1;
	protected QuickAdapter<Found> LostAdapter;// ʧ��

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.activity_find_lost, container,
				false);
		lv_1 = (ListView) view.findViewById(R.id.lv_lost);
		get_find_list_from_bmobsql();
		return view;
	}

	public void initData() {
		// TODO Auto-generated method stub
		if (LostAdapter == null) {
			LostAdapter = new QuickAdapter<Found>(getActivity(),
					R.layout.item_list_find) {
				@Override
				protected void convert(BaseAdapterHelper helper, Found lost) {
					helper.setText(R.id.tv_find_list_title, lost.getTitle())
							.setText(R.id.tv_find_list_miaoshu,
									lost.getDescribe())
							.setText(R.id.tv_find_list_time,
									lost.getCreatedAt())
							.setText(R.id.tv_find_list_phone_number,
									lost.getPhone());
				}
			};
		}
	}

	public void get_find_list_from_bmobsql() {
		initData();
		BmobQuery<Found> query = new BmobQuery<Found>();
		query.order("-createdAt");// ����ʱ�併��
		query.findObjects(getActivity(), new FindListener<Found>() {

			@Override
			public void onSuccess(List<Found> losts) {
				// TODO Auto-generated method stub
				LostAdapter.clear();
				if (losts == null || losts.size() == 0) {
					LostAdapter.notifyDataSetChanged();
					return;
				}
				LostAdapter.addAll(losts);
				lv_1.setAdapter(LostAdapter);
			}

			@Override
			public void onError(int code, String arg0) {
				// TODO Auto-generated method stub
			}
		});
	}
}
