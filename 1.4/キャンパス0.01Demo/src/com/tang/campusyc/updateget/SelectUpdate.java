package com.tang.campusyc.updateget;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.GetListener;

import com.tang.campusyc.data_claaback.TallDataCallBack;
import com.tang.campusyc.errorlog.ErrorGetActivity;
import com.tang.campusyc.toast.DiyDialog_Error_get;
import com.tang.campusyc.toast.DiyToast;

/**
 * @author Administrator
 * @year 2019
 * @Todo TODO 检查版本
 * @package_name com.tang.campusyc.updateget
 * @project_name キャンパス0.01Demo
 * @file_name SelectUpdate.java
 */
public class SelectUpdate {

	public static String now_version = "1.4";// 当前编译版本

	// 此时云端Bmob数据库中的版本号应该比当前版本高，才会弹出对话框提示用户更新
	// 比如当前1.2，云端1.3，就会提示用户更新版本

	public static void get_version(final Context context) {
		// TODO Auto-generated method stub
		final ProgressDialog progressDialog = new ProgressDialog(context);
		progressDialog.setMessage("正在判断当前版本......");
		progressDialog.setCancelable(false);
		progressDialog.setIcon(android.R.drawable.ic_dialog_info);
		progressDialog.show();
		// 判断当前版本号
		try {
			BmobQuery<TallDataCallBack> bmobQuery = new BmobQuery<TallDataCallBack>();
			bmobQuery.getObject(context, "43ee5fbfec",
					new GetListener<TallDataCallBack>() {

						@Override
						public void onFailure(int arg0, String arg1) {
							// TODO Auto-generated method stub
							DiyToast.showToast(context, "错误：" + arg1);
							progressDialog.dismiss();
							DiyDialog_Error_get.showDialog_QQ(context, "错误代码："
									+ String.valueOf(arg0), arg1, "取消");
						}

						@Override
						public void onSuccess(TallDataCallBack arg0) {
							// TODO Auto-generated method stub
							if (Float.valueOf(arg0.getAddress().toString()) > Float
									.valueOf(now_version)) {
								progressDialog.dismiss();
								DiyToast.showToast(context, "有新版本，请更新");
								AlertDialog.Builder builder = new AlertDialog.Builder(
										context);
								builder.setTitle("更新")
										.setCancelable(false)
										.setMessage(
												"当前版本："
														+ now_version
														+ "\n"
														+ "最新版本："
														+ arg0.getAddress()
																.toString()
														+ "\n"
														+ "请点击确定后在浏览器中下载更新")
										.setPositiveButton(
												"确定",
												new DialogInterface.OnClickListener() {

													@Override
													public void onClick(
															DialogInterface dialog,
															int which) {
														// TODO Auto-generated
														// method stub
														Uri uri = Uri
																.parse("https://naiyouhuameitang.club/apks/updateDemo.apk");
														Intent intent = new Intent(
																Intent.ACTION_VIEW,
																uri);
														context.startActivity(intent);
													}
												})
										.setNegativeButton("下次一定", null).show();
							} else {
								progressDialog.dismiss();
							}
						}
					});
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("error", e.toString());
		}
	}
}
