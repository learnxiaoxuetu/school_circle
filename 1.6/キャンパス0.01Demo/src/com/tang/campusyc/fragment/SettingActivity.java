package com.tang.campusyc.fragment;

import java.text.SimpleDateFormat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tang.campusyc.LoginActivity;
import com.tang.campusyc.R;
import com.tang.campusyc.toast.DiyToast;
import com.tang.campusyc.updateget.SelectUpdate;

public class SettingActivity extends Activity {
	private LinearLayout line_setting_exit, line_setting_seeupdate;
	private TextView tv_now_login_username;
	private int number = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_setting);
		initView();
		LoginActivity.sharedPreferences = getSharedPreferences("rember",
				MODE_WORLD_WRITEABLE);
		tv_now_login_username.setText(LoginActivity.sharedPreferences
				.getString("now_user", null));
		line_setting_exit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DiyToast.showToast(getApplicationContext(),
						"暂时不可用，如果想退出、切换账号，请清除本APP的数据");
			}
		});
		line_setting_seeupdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SelectUpdate.get_version(SettingActivity.this);
			}
		});
		get_now_time();
	}

	private void initView() {
		// TODO Auto-generated method stub
		line_setting_seeupdate = (LinearLayout) findViewById(R.id.line_setting_seeupdate);
		tv_now_login_username = (TextView) findViewById(R.id.tv_now_login_username);
		line_setting_exit = (LinearLayout) findViewById(R.id.line_setting_exit);
	}

	private void get_now_time() {
		// TODO Auto-generated method stub
		String time;// 时间
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HHmm");// 获取时间
		time = simpleDateFormat.format(new java.util.Date());
		System.out.println(time);// 输出日志用于调试
		if (Float.valueOf(time) > 1200 && Float.valueOf(time) < 1300) {// 大于12点小于13点
			tv_now_login_username.setText(LoginActivity.sharedPreferences
					.getString("now_user", null) + "，中午好");
		}
		if (Float.valueOf(time) > 0600 && Float.valueOf(time) < 1200) {// 大于6点小于12点
			tv_now_login_username.setText(LoginActivity.sharedPreferences
					.getString("now_user", null) + "，上午好");
		}
		if (Float.valueOf(time) > 1300 && Float.valueOf(time) < 1800) {// 大于13点小于18点
			tv_now_login_username.setText(LoginActivity.sharedPreferences
					.getString("now_user", null) + "，下午好");
		}
		if (Float.valueOf(time) > 1800) {// 小于18点
			tv_now_login_username.setText(LoginActivity.sharedPreferences
					.getString("now_user", null) + "，晚上好");
		}
	}
}
