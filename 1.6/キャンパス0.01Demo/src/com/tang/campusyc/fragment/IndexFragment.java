package com.tang.campusyc.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;

import com.tang.campusyc.R;

public class IndexFragment extends Fragment {
	private WebView web_view_top;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_index, container, false);
		initView(view);
		web_view_top
				.loadUrl("https://ws1.sinaimg.cn/large/005ZFreDly1g95dofzuvaj31hc0u0akl.jpg");
		// 支持javascript
		web_view_top.getSettings().setJavaScriptEnabled(true);
		// 设置可以支持缩放
		web_view_top.getSettings().setSupportZoom(false);
		// 设置出现缩放工具
		web_view_top.getSettings().setBuiltInZoomControls(true);
		// 扩大比例的缩放
		web_view_top.getSettings().setUseWideViewPort(true);
		// 自适应屏幕
		web_view_top.getSettings().setLayoutAlgorithm(
				LayoutAlgorithm.SINGLE_COLUMN);
		web_view_top.getSettings().setLoadWithOverviewMode(true);

		return view;
	}

	private void initView(View view) {
		// TODO Auto-generated method stub
		web_view_top = (WebView) view.findViewById(R.id.web_view_top);
	}
}