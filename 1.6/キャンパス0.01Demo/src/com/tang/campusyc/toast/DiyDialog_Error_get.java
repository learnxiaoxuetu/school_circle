package com.tang.campusyc.toast;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

public class DiyDialog_Error_get {
	public static void showDialog(Context context, String title,
			String message, String button_con) {
		new AlertDialog.Builder(context).setTitle(title).setMessage(message)
				.setPositiveButton(button_con, null).show();
	}

	public static void showDialog_QQ(final Context context, String title,
			String message, String button_con) {
		new AlertDialog.Builder(context)
				.setTitle(title)
				.setMessage(message)
				.setPositiveButton(button_con, null)
				.setNegativeButton("联系开发者",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								try {
									DiyToast.showToast(context, "跳转添加qq");
									// 第二种方式：可以跳转到添加好友，如果qq号是好友了，直接聊天
									String url = "mqqwpa://im/chat?chat_type=wpa&uin=1097681347";// uin是发送过去的qq号码
									context.startActivity(new Intent(
											Intent.ACTION_VIEW, Uri.parse(url)));
								} catch (Exception e) {
									e.printStackTrace();
									DiyToast.showToast(context, "请检查是否安装QQ");
								}
							}
						}).show();
	}

}
