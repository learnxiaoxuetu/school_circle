package com.tang.campusyc.data_util_helper;

import cn.bmob.v3.BmobObject;

/**
 * @author Administrator
 * @year 2019
 * @Todo TODO 丢失
 * @package_name com.tang.campusyc.data_util_helper
 * @project_name キャンパス0.01Demo
 * @file_name Lost.java
 */
public class Lost extends BmobObject {

	private String title;// 标题
	private String describe;// 描述
	private String phone;// 联系手机

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}
