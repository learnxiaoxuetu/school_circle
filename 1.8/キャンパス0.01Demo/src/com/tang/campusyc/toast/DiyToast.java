package com.tang.campusyc.toast;

import com.tang.campusyc.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Administrator
 * @year 2019
 * @Todo TODO 自定义Toast
 * @package_name com.tang.campusyc.toast
 * @project_name キャンパス0.01Demo
 * @file_name DiyToast.java
 */
public class DiyToast {
	public static Toast toast;// toast

	public static void showToast(Context context, String string) {
		View toastRoot = LayoutInflater.from(context).inflate(
				R.layout.my_toast, null, false);// 新建View并绑定
		TextView tv = (TextView) toastRoot
				.findViewById(R.id.tv_toast_show_info);// 新建Textview并绑定
		if (toast == null) {// 当Toast为空（即Toast没有在显示中）
			toast = Toast.makeText(context, string, Toast.LENGTH_SHORT);
			toast.setView(toastRoot);// 设置View
			tv.setText(string);// 设置文本
		} else {
			toast.setView(toastRoot);// 设置View
			tv.setText(string);// 设置文本
		}
		toast.show();// 显示Toast
	}
}
