package com.tang.campusyc;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.GetListener;

import com.tang.campusyc.data_util_helper.TallDataCallBack;
import com.tang.campusyc.toast.DiyToast;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

/**
 * @author Administrator
 * @year 2019
 * @Todo TODO 关于
 * @package_name com.tang.campusyc
 * @project_name キャンパス0.01Demo
 * @file_name AboutActivity.java
 */
public class AboutActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		((TextView) findViewById(R.id.tv_about_update_log))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Uri uri = Uri
								.parse("https://naiyouhuameitang.club/school_circle.html");
						Intent intent = new Intent(Intent.ACTION_VIEW, uri);
						startActivity(intent);
					}
				});
		BmobQuery<TallDataCallBack> bmobQuery = new BmobQuery<TallDataCallBack>();
		bmobQuery.getObject(AboutActivity.this, "43ee5fbfec",
				new GetListener<TallDataCallBack>() {

					@Override
					public void onFailure(int arg0, String arg1) {
						// TODO Auto-generated method stub
						DiyToast.showToast(getApplicationContext(), arg1);
					}

					@Override
					public void onSuccess(TallDataCallBack arg0) {
						// TODO Auto-generated method stub
						((TextView) findViewById(R.id.tv_about_version))
								.setText("当前最新版本：ver."
										+ arg0.getAddress().toString());
					}
				});
	}
}
