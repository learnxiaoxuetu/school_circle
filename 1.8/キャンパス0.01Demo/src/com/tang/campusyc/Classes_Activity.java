package com.tang.campusyc;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.tang.campusyc.R;
import com.tang.campusyc.toast.DiyToast;

/**
 * @author Administrator
 * @year 2019
 * @Todo TODO 课程表
 * @package_name com.tang.campusyc
 * @project_name キャンパス0.01Demo
 * @file_name Classes_Activity.java
 */
public class Classes_Activity extends Activity {
	private int colors[] = { Color.rgb(0xee, 0xff, 0xff),
			Color.rgb(0xf0, 0x96, 0x09), Color.rgb(0x8c, 0xbf, 0x26),
			Color.rgb(0x00, 0xab, 0xa9), Color.rgb(0x99, 0x6c, 0x33),
			Color.rgb(0x3b, 0x92, 0xbc), Color.rgb(0xd5, 0x4d, 0x34),
			Color.rgb(0xcc, 0xcc, 0xcc), Color.rgb(0x4a, 0xdd, 0x03) };
	private LinearLayout ll1;
	private LinearLayout ll2;
	private LinearLayout ll3;
	private LinearLayout ll4;
	private LinearLayout ll5;
	private Button btn_clases_table_select;
	private SharedPreferences sharedPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_classes_index);
		initView();// 绑定控件
		// 切换课程
		btn_clases_table_select.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog_show_select();
			}
		});
	}

	private void dialog_show_select() {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				Classes_Activity.this);
		View view = LayoutInflater.from(Classes_Activity.this).inflate(
				R.layout.dialog_show_select_classes_table, null, false);
		final RadioButton ra_jiangjiao_1923_kuaiji = (RadioButton) view
				.findViewById(R.id.ra_jiangjiao_1923_kuaiji);
		final RadioButton ra_shanxian_1809_jisuanjijichu = (RadioButton) view
				.findViewById(R.id.ra_shanxian_1809_jisuanjijichu);
		if (sharedPreferences.getBoolean("jiaojiang_kuaiji_1923", false) == true) {
			ra_jiangjiao_1923_kuaiji.setChecked(true);
		}
		if (sharedPreferences.getBoolean("shanxian_jisuanji_1809", false) == true) {
			ra_shanxian_1809_jisuanjijichu.setChecked(true);
		}
		builder.setTitle("请选择下列已经支持的学校");
		builder.setMessage("没有支持到的学校、班级，请将课程表拍照、截图、XML格式文档，发送邮件到1097681347@qq.com，或者添加QQ:1097681347，多谢合作！");
		builder.setView(view);
		builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if (ra_jiangjiao_1923_kuaiji.isChecked()) {
					sharedPreferences.edit()
							.putBoolean("jiaojiang_kuaiji_1923", true).commit();
					//
					DiyToast.showToast(getApplicationContext(),
							"下次启动时将切换到：椒江职中会计1923");
					//
					sharedPreferences.edit()
							.putBoolean("shanxian_jisuanji_1809", false)
							.commit();
				}
				if (ra_shanxian_1809_jisuanjijichu.isChecked()) {
					sharedPreferences.edit()
							.putBoolean("shanxian_jisuanji_1809", true)
							.commit();
					//
					DiyToast.showToast(getApplicationContext(),
							"下次启动时将切换到：单县职中计算机基础1809");
					//
					sharedPreferences.edit()
							.putBoolean("jiaojiang_kuaiji_1923", false)
							.commit();
				}
			}
		});
		builder.show();
	}

	// 单县中职
	private void class_for_shanxian_jisuanji_1809() {
		// TODO Auto-generated method stub
		/** 周一 **/
		setClass(ll1, "早自习", "全班", "7:40-8:10", "每天", 1, 2);
		setClass(ll1, "语文", "何伦海", "8:20-:9:50", "", 2, 3);
		setClass(ll1, "基础会计", "沈盈盈", "10:20-11:50", "", 2, 4);
		setClass(ll1, "凭证填审", "8号机房/毛利平", "14:10-15:50", "", 2, 5);
		setClass(ll1, "班会", "全班", "16:00-16:40", "", 1, 6);
		setClass(ll1, "晚自习", "全班", "18:00-21:00", "每天", 3, 7);
	}

	// 椒江中职
	private void class_for_jiaojiang_kuaiji_1923() {
		// TODO Auto-generated method stub
		/** 周一 **/
		setClass(ll1, "早自习", "全班", "7:40-8:10", "每天", 1, 2);
		setClass(ll1, "语文", "何伦海", "8:20-:9:50", "", 2, 3);
		setClass(ll1, "基础会计", "沈盈盈", "10:20-11:50", "", 2, 4);
		setClass(ll1, "凭证填审", "8号机房/毛利平", "14:10-15:50", "", 2, 5);
		setClass(ll1, "班会", "全班", "16:00-16:40", "", 1, 6);
		setClass(ll1, "晚自习", "全班", "18:00-21:00", "每天", 3, 7);
		/** 周二 **/
		setClass(ll2, "早自习", "全班", "7:40-8:10", "每天", 1, 2);
		setClass(ll2, "计算机基础", "8号机房/金鹏洲", "8:20-:9:50", "", 2, 6);
		setClass(ll2, "政治", "杨春红", "10:20-11:50", "", 2, 2);
		setClass(ll2, "凭证填审", "毛利平", "14:10-14:50", "", 1, 5);
		setClass(ll2, "点钞与键盘", "实训6302/徐青欣", "15:00-16:40", "", 2, 3);
		setClass(ll2, "晚自习", "全班", "18:00-21:00", "每天", 3, 7);
		/** 周三 **/
		setClass(ll3, "早自习", "全班", "7:40-8:10", "每天", 1, 2);
		setClass(ll3, "基础会计", "沈盈盈", "8:20-:9:50", "", 2, 4);
		setClass(ll3, "语文", "何伦海", "10:20-11:50", "", 2, 3);
		setClass(ll3, "英语", "冯凌红", "14:10-14:50", "", 2, 2);
		setClass(ll3, "数学", "周宇斌", "15:00-16:40", "", 1, 4);
		setClass(ll3, "晚自习", "全班", "18:00-21:00", "每天", 3, 7);
		/** 周四 **/
		setClass(ll4, "早自习", "全班", "7:40-8:10", "每天", 1, 2);
		setClass(ll4, "基础会计", "沈盈盈", "8:20-:9:50", "", 2, 4);
		setClass(ll4, "凭证填审", "8号机房/毛利平", "10:20-11:50", "", 2, 5);
		setClass(ll4, "英语", "冯凌红", "14:10-14:50", "", 2, 2);
		setClass(ll4, "数学", "周宇斌", "15:00-16:40", "", 1, 4);
		setClass(ll4, "晚自习", "全班", "18:00-21:00", "每天", 3, 7);
		/** 周五 **/
		setClass(ll5, "早自习", "全班", "7:40-8:10", "每天", 1, 2);
		setClass(ll5, "数学", "周宇斌", "8:20-:9:50", "", 2, 4);
		setClass(ll5, "计算机基础", "8号机房/金鹏洲", "10:20-11:50", "", 2, 6);
		setClass(ll5, "体育健康", "胡传国", "14:10-14:50", "", 2, 1);
		setClass(ll5, "教师会", "全班", "15:00-16:40", "", 1, 1);
		setClass(ll5, "晚自习", "全班", "18:00-21:00", "每天", 3, 7);
	}

	public static int dip2px(Context context, float dpValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dpValue * scale + 0.5f);
	}

	/** * 根据手机的分辨率从 px(像素) 的单位 转成为 dp */
	public static int px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}

	/**
	 * @param ll
	 *            布局
	 * @param title
	 *            名称
	 * @param place
	 *            地点
	 * @param time
	 *            时间
	 * @param week
	 *            周次
	 * @param classes
	 *            节数
	 * @param color
	 *            背景色
	 */
	private void setClass(LinearLayout ll, String title, String place,
			String time, String week, int classes, int color) {
		View view = LayoutInflater.from(this).inflate(
				R.layout.item_classes_table, null, false);
		// 1节课
		if (classes == 1) {
			view.setMinimumHeight(dip2px(this, classes * 72));
		}
		// 2节课
		if (classes == 2) {
			view.setMinimumHeight(dip2px(this, classes * 73));
		}
		// 3节课
		if (classes == 3) {
			view.setMinimumHeight(dip2px(this, 220));
		}
		// 设置View的背景
		view.setBackgroundColor(colors[color]);
		// 设置课程信息文本
		((TextView) view.findViewById(R.id.tv_class_title)).setText(title);
		((TextView) view.findViewById(R.id.tv_class_time)).setText(time);
		((TextView) view.findViewById(R.id.tv_class_week)).setText(week);
		((TextView) view.findViewById(R.id.tv_class_place)).setText(place);
		// 设置顶部、底部隔离空文字
		TextView nTextView1 = new TextView(this);
		TextView nTextView2 = new TextView(this);
		nTextView1.setHeight(1);
		nTextView2.setHeight(1);
		// 设置ll（LineayLayout）布局样式
		ll.addView(nTextView1);
		ll.addView(view);
		ll.addView(nTextView2);
	}

	/**
	 * 绑定控件
	 */
	private void initView() {
		// TODO Auto-generated method stub
		btn_clases_table_select = (Button) findViewById(R.id.btn_clases_table_select);
		ll1 = (LinearLayout) findViewById(R.id.ll1);
		ll2 = (LinearLayout) findViewById(R.id.ll2);
		ll3 = (LinearLayout) findViewById(R.id.ll3);
		ll4 = (LinearLayout) findViewById(R.id.ll4);
		ll5 = (LinearLayout) findViewById(R.id.ll5);
		sharedPreferences = getSharedPreferences("rember_auto_class_table",
				MODE_WORLD_WRITEABLE);
		if (sharedPreferences != null) {
			if (sharedPreferences.getBoolean("jiaojiang_kuaiji_1923", false) == true) {
				class_for_jiaojiang_kuaiji_1923();
			} else if (sharedPreferences.getBoolean("shanxian_jisuanji_1809",
					false) == true) {
				class_for_shanxian_jisuanji_1809();
			} else {
				DiyToast.showToast(getApplicationContext(), "请选择学校（向左滑会出现选项）");
			}
		}
	}
}
