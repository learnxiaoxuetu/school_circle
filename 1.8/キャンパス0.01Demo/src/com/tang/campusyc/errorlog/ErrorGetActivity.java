package com.tang.campusyc.errorlog;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.webkit.WebView;

import com.tang.campusyc.R;

/**
 * @author Administrator
 * @year 2019
 * @Todo TODO 给微信API发送新用户注册通知
 * @package_name com.tang.campusyc.errorlog
 * @project_name キャンパス0.01Demo
 * @file_name ErrorGetActivity.java
 */
public class ErrorGetActivity extends Activity {
	public static String errorlog_get = "";
	private String SKEY = "SCU66788Tac2bf7385575174e067c917d471e25365dd3983cec5ee";
	private String web_index = "sc.ftqq.com";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_error_get);
		test(errorlog_get);
	}

	private void test(String errorlog) {
		WebView wv = (WebView) findViewById(R.id.web_error_log);
		wv.loadUrl("https://" + web_index + "/" + SKEY
				+ ".send?text=【APP客户端】收到一条日志 " + "-------" + errorlog);
		finish();
	}
}
