package com.tang.campusyc;

import com.tang.campusyc.errorlog.ErrorGetActivity;
import com.tang.campusyc.toast.DiyToast;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Message_reback_Activity extends Activity {
	private Button btn_mesesage_send;
	private EditText et_mess_name, et_mess_mail, et_mess_phone,
			et_mess_message;
	private String name, mail, phone, message;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message_reback);
		initView();
		// 点击确定按钮
		btn_mesesage_send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				name = et_mess_name.getText().toString();
				mail = et_mess_mail.getText().toString();
				phone = et_mess_phone.getText().toString();
				message = et_mess_message.getText().toString();
				if (name.isEmpty()) {
					DiyToast.showToast(getApplicationContext(), "请输入昵称");
				} else if (mail.isEmpty()) {
					DiyToast.showToast(getApplicationContext(), "请输入邮箱");
				} else if (phone.isEmpty()) {
					DiyToast.showToast(getApplicationContext(), "请输入电话号码");
				} else if (message.isEmpty()) {
					DiyToast.showToast(getApplicationContext(), "请输入反馈内容");
				} else {
					DiyToast.showToast(getApplicationContext(), "提交成功");
					ErrorGetActivity.errorlog_get = "【反馈】昵称：" + name + "邮箱："
							+ mail + "电话：" + phone + "内容：" + message;
					finish();
				}
			}
		});
	}

	private void initView() {
		// TODO Auto-generated method stub
		btn_mesesage_send = (Button) findViewById(R.id.btn_mesesage_send);
		et_mess_mail = (EditText) findViewById(R.id.et_mess_mail);
		et_mess_message = (EditText) findViewById(R.id.et_mess_message);
		et_mess_name = (EditText) findViewById(R.id.et_mess_name);
		et_mess_phone = (EditText) findViewById(R.id.et_mess_phone);
	}
}
