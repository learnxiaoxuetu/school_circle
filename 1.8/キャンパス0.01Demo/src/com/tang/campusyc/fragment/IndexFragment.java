package com.tang.campusyc.fragment;

import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;

import com.baidu.voicerecognition.android.s;
import com.tang.campusyc.Classes_Activity;
import com.tang.campusyc.Message_reback_Activity;
import com.tang.campusyc.Note_Book_Activity;
import com.tang.campusyc.R;
import com.tang.campusyc.Time_Table_Activity;
import com.tang.campusyc.adapter.BaseAdapterHelper;
import com.tang.campusyc.adapter.QuickAdapter;
import com.tang.campusyc.data_util_helper.Found;
import com.tang.campusyc.data_util_helper.News;
import com.tang.campusyc.toast.DiyToast;

/**
 * @author Administrator
 * @year 2019
 * @Todo TODO 程序首页
 * @package_name com.tang.campusyc.fragment
 * @project_name キャンパス0.01Demo
 * @file_name IndexFragment.java
 */
public class IndexFragment extends Fragment {
	private Button btn_class_table, btn_blog, btn_time_table,
			btn_message_reback, btn_note_rember, btn_get_sign;
	private TextView tv_count_time_for_jiaqi;
	private TextView tv_now_day;
	private ImageView img_date;
	private static ListView list_index_news;
	public static QuickAdapter<News> NEWSAdapter;// 新闻

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_index, container, false);
		initView(view);// 绑定控件
		setTime();
		// 显示课程表
		btn_class_table.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub\
				startActivity(new Intent(getActivity(), Classes_Activity.class));
			}
		});
		// 头像图片点击
		img_date.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
		// 时刻表
		btn_time_table.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(getActivity(),
						Time_Table_Activity.class));
			}
		});
		// 述说
		btn_blog.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DiyToast.showToast(getActivity(), "公开测试期间不开放“述说”功能，请原谅");
			}
		});
		btn_message_reback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(getActivity(),
						Message_reback_Activity.class));
			}
		});
		btn_note_rember.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(getActivity(),
						Note_Book_Activity.class));
			}
		});
		btn_get_sign.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				DiyToast.showToast(getActivity(), "相机唤醒失败，无法扫描签到码。请给予相机权限");
			}
		});
		get_find_list_from_bmobsql(getActivity());
		return view;
	}

	/**
	 * 设置校历中日期的时间
	 */
	public void setTime() {
		Calendar calendar = Calendar.getInstance();
		String year = calendar.get(Calendar.YEAR) + "";
		String month = calendar.get(Calendar.MONTH) + 1 + "";
		String day = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String week = calendar.get(Calendar.WEEK_OF_YEAR) + "";
		String dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) + "";
		tv_count_time_for_jiaqi.setText(year + "年 第 " + week + " 周 " + " "
				+ " 星期 " + dayOfWeek);
		tv_now_day.setText(month + "  月  " + day + "  日  ");
	}

	private void initView(View view) {
		// TODO Auto-generated method stub
		tv_now_day = (TextView) view.findViewById(R.id.tv_now_day);
		img_date = (ImageView) view.findViewById(R.id.img_date);
		btn_class_table = (Button) view.findViewById(R.id.btn_class_table);
		tv_count_time_for_jiaqi = (TextView) view
				.findViewById(R.id.tv_count_time_for_jiaqi);
		btn_blog = (Button) view.findViewById(R.id.btn_blog);
		btn_time_table = (Button) view.findViewById(R.id.btn_time_table);
		list_index_news = (ListView) view.findViewById(R.id.list_index_news);
		btn_message_reback = (Button) view
				.findViewById(R.id.btn_message_reback);
		btn_note_rember = (Button) view.findViewById(R.id.btn_note_rember);
		btn_get_sign = (Button) view.findViewById(R.id.btn_get_sign);
	}

	public static void initData(Context context) {
		// TODO Auto-generated method stub
		if (NEWSAdapter == null) {
			NEWSAdapter = new QuickAdapter<News>(context,
					R.layout.list_index_news) {
				@Override
				protected void convert(BaseAdapterHelper helper, News lost) {
					helper.setText(R.id.tv_news_title, lost.getTitle())
							.setText(R.id.tv_news_miaoshu, lost.getDescribe())
							.setText(R.id.tv_news_time, lost.getCreatedAt());
				}
			};
		}
	}

	private static void get_find_list_from_bmobsql(final Context context) {
		initData(context);
		BmobQuery<News> query = new BmobQuery<News>();
		query.order("-createdAt");// 按照时间降序
		query.findObjects(context, new FindListener<News>() {

			@Override
			public void onSuccess(List<News> newList) {
				// TODO Auto-generated method stub
				NEWSAdapter.clear();
				if (newList == null || newList.size() == 0) {
					NEWSAdapter.notifyDataSetChanged();
					return;
				}
				NEWSAdapter.addAll(newList);
				list_index_news.setAdapter(NEWSAdapter);
			}

			@Override
			public void onError(int code, String arg0) {
				// TODO Auto-generated method stub
			}
		});
	}
}