package com.tang.campusyc.fragment;

import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;

import com.tang.campusyc.R;
import com.tang.campusyc.adapter.BaseAdapterHelper;
import com.tang.campusyc.adapter.QuickAdapter;
import com.tang.campusyc.data_util_helper.Found;

/**
 * @author Administrator
 * @year 2019
 * @Todo TODO 捡到
 * @package_name com.tang.campusyc.fragment
 * @project_name キャンパス0.01Demo
 * @file_name FoundFragment.java
 */
public class FoundFragment extends Fragment {
	private static ListView lv_1;
	public static QuickAdapter<Found> LostAdapter;// 失物

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.activity_find_lost, container,
				false);
		lv_1 = (ListView) view.findViewById(R.id.lv_lost);
		get_find_list_from_bmobsql(getActivity());
		return view;
	}

	public static void initData(Context context) {
		// TODO Auto-generated method stub
		if (LostAdapter == null) {
			LostAdapter = new QuickAdapter<Found>(context,
					R.layout.item_list_find) {
				@Override
				protected void convert(BaseAdapterHelper helper, Found lost) {
					helper.setText(R.id.tv_find_list_title, lost.getTitle())
							.setText(R.id.tv_find_list_miaoshu,
									lost.getDescribe())
							.setText(R.id.tv_find_list_time,
									lost.getCreatedAt())
							.setText(R.id.tv_find_list_phone_number,
									lost.getPhone());
				}
			};
		}
	}

	public static void get_find_list_from_bmobsql(Context context) {
		initData(context);
		BmobQuery<Found> query = new BmobQuery<Found>();
		query.order("-createdAt");// 按照时间降序
		query.findObjects(context, new FindListener<Found>() {

			@Override
			public void onSuccess(List<Found> losts) {
				// TODO Auto-generated method stub
				LostAdapter.clear();
				if (losts == null || losts.size() == 0) {
					LostAdapter.notifyDataSetChanged();
					return;
				}
				LostAdapter.addAll(losts);
				lv_1.setAdapter(LostAdapter);
			}

			@Override
			public void onError(int code, String arg0) {
				// TODO Auto-generated method stub
			}
		});
	}
}