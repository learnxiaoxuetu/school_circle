package com.tang.campusyc.fragment;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import cn.bmob.v3.listener.SaveListener;

import com.baidu.voicerecognition.android.b;
import com.baidu.voicerecognition.android.ui.BaiduASRDigitalDialog;
import com.baidu.voicerecognition.android.ui.DialogRecognitionListener;
import com.example.codeforpass3.CodeView;
import com.tang.campusyc.LoginActivity;
import com.tang.campusyc.R;
import com.tang.campusyc.api.Config;
import com.tang.campusyc.api.Constants;
import com.tang.campusyc.data_util_helper.Found;
import com.tang.campusyc.data_util_helper.Lost;
import com.tang.campusyc.toast.DiyToast;

/**
 * @author Administrator
 * @year 2019
 * @Todo TODO 发布界面
 * @package_name com.tang.campusyc.fragment.othres.activity
 * @project_name キャンパス0.01Demo
 * @file_name HelpFragment.java
 */
public class HelpFragment extends Fragment implements OnClickListener {
	private EditText et_title_find;
	private EditText et_phone_find;
	private EditText et_miaoshu_find;
	private Button btn_find_con;
	private Button btn_find_resert;
	private String title, phone, miaoshu;
	private TextView tv_last_number;
	private int last_number = 200;
	private Spinner sp_get_lost;
	private static final String TAG = "demoActivity";
	private EditText mResult = null;
	private BaiduASRDigitalDialog mDialog = null;
	private DialogRecognitionListener mRecognitionListener;
	private int mCurrentTheme = Config.DIALOG_THEME;
	private Button start_diolog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.activity_find_help, container,
				false);
		initView(view);// 绑定
		edit_set_number();// 字数
		check_login_user();// 检查当前登录用户
		mRecognitionListener = new DialogRecognitionListener() {
			@Override
			public void onResults(Bundle results) {
				ArrayList<String> rs = results != null ? results
						.getStringArrayList(RESULTS_RECOGNITION) : null;
				if (rs != null && rs.size() > 0) {
					mResult.setText(rs.get(0));
				}

			}
		};
		return view;
	}

	private void check_login_user() {
		LoginActivity.sharedPreferences = getActivity().getSharedPreferences(
				"rember", getActivity().MODE_WORLD_WRITEABLE);
		et_phone_find.setHint("");
		et_phone_find.setClickable(false);
		et_phone_find.setFocusable(false);
		if (LoginActivity.sharedPreferences.getString("now_user", null)
				.isEmpty()) {
			DiyToast.showToast(getActivity(), "要不。。。先登录？");
		} else {
			et_phone_find.setHint(LoginActivity.sharedPreferences.getString(
					"now_user", null));
		}
	}

	private void initView(View view) {
		// TODO Auto-generated method stub
		sp_get_lost = (Spinner) view.findViewById(R.id.sp_lost_find);
		btn_find_con = (Button) view.findViewById(R.id.btn_find_con);
		btn_find_resert = (Button) view.findViewById(R.id.btn_find_resert);
		et_miaoshu_find = (EditText) view.findViewById(R.id.et_miaoshu_find);
		et_phone_find = (EditText) view.findViewById(R.id.et_phone_find);
		et_title_find = (EditText) view.findViewById(R.id.et_title_find);
		btn_find_con.setOnClickListener(this);
		btn_find_resert.setOnClickListener(this);
		mResult = (EditText) view.findViewById(R.id.et_miaoshu_find);
		start_diolog = (Button) view.findViewById(R.id.start_diolog);
		start_diolog.setOnClickListener(this);
		tv_last_number = (TextView) view.findViewById(R.id.tv_last_number);
		tv_last_number.setText("200");
	}

	private void edit_set_number() {
		et_miaoshu_find.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (start > 200) {
					DiyToast.showToast(getActivity(), "最高200字~");
					tv_last_number.setTextColor(Color.RED);

				} else {
					tv_last_number.setTextColor(Color.BLACK);
				}
				tv_last_number.setText(String
						.valueOf((last_number - 1) - start));
				Editable editable = et_phone_find.getText();
				Selection.setSelection(editable, editable.length());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
	}

	/**
	 * 手机号号段校验， 第1位：1； 第2位：{3、4、5、6、7、8}任意数字； 第3—11位：0—9任意数字
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isTelPhoneNumber(String value) {
		if (value != null && value.length() == 11) {
			Pattern pattern = Pattern.compile("^1[3|4|5|6|7|8][0-9]\\d{8}$");
			Matcher matcher = pattern.matcher(value);
			return matcher.matches();
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_find_con:
			title = et_title_find.getText().toString();
			miaoshu = et_miaoshu_find.getText().toString();
			phone = et_phone_find.getHint().toString();
			if (title.isEmpty() || phone.isEmpty() || miaoshu.isEmpty()) {
				DiyToast.showToast(getActivity(), "不能有空白项，更详细的描述才能帮你找回你的东西！");
			} else {
				if (isTelPhoneNumber(phone)) {
					show_dialog_for_repass();
				} else {
					DiyToast.showToast(getActivity(), "您的电话号码似乎错误");
				}
			}
			break;
		case R.id.btn_find_resert:
			et_miaoshu_find.setText(null);
			et_phone_find.setText(null);
			et_title_find.setText(null);
			break;
		case R.id.start_diolog:
			// mResult.setText(null);
			if (mDialog == null || mCurrentTheme != Config.DIALOG_THEME) {
				mCurrentTheme = Config.DIALOG_THEME;
				if (mDialog != null) {
					mDialog.dismiss();
				}
				Bundle params = new Bundle();
				params.putString(BaiduASRDigitalDialog.PARAM_API_KEY,
						Constants.API_KEY);
				params.putString(BaiduASRDigitalDialog.PARAM_SECRET_KEY,
						Constants.SECRET_KEY);
				params.putInt(BaiduASRDigitalDialog.PARAM_DIALOG_THEME,
						Config.DIALOG_THEME);
				mDialog = new BaiduASRDigitalDialog(getActivity(), params);
				mDialog.setDialogRecognitionListener(mRecognitionListener);
			}
			mDialog.getParams().putInt(BaiduASRDigitalDialog.PARAM_PROP,
					Config.CURRENT_PROP);
			mDialog.getParams().putString(BaiduASRDigitalDialog.PARAM_LANGUAGE,
					Config.getCurrentLanguage());
			mDialog.getParams().putBoolean(
					BaiduASRDigitalDialog.PARAM_START_TONE_ENABLE,
					Config.PLAY_START_SOUND);
			mDialog.getParams().putBoolean(
					BaiduASRDigitalDialog.PARAM_END_TONE_ENABLE,
					Config.PLAY_END_SOUND);
			mDialog.getParams().putBoolean(
					BaiduASRDigitalDialog.PARAM_TIPS_TONE_ENABLE,
					Config.DIALOG_TIPS_SOUND);
			mDialog.show();
			break;
		default:
			break;
		}
	}

	private void con_for_find_lost() {
		// TODO Auto-generated method stub
		if (sp_get_lost.getSelectedItem().toString().equals("我捡到了东西")) {
			new AlertDialog.Builder(getActivity())
					.setTitle("提示")
					.setMessage("你确定要发布“我捡到了东西”吗？")
					.setPositiveButton("确定",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method
									// stub
									FoundFragment.initData(getActivity());
									Found found = new Found();
									found.setDescribe(miaoshu);
									found.setPhone(phone);
									found.setTitle(title);
									found.save(getActivity(),
											new SaveListener() {

												@Override
												public void onSuccess() {
													// TODO
													// Auto-generated
													// method stub
													DiyToast.showToast(
															getActivity(),
															"发布成功");
													FoundFragment
															.get_find_list_from_bmobsql(getActivity());
													FindFragment
															.get_find_list_from_bmobsql(getActivity());
													et_miaoshu_find
															.setText(null);
													et_phone_find.setText(null);
													et_title_find.setText(null);
												}

												@Override
												public void onFailure(int code,
														String arg0) {
													// TODO
													// Auto-generated
													// method stub
													DiyToast.showToast(
															getActivity(),
															"失败：" + arg0);
												}
											});
								}
							}).setNegativeButton("取消", null).show();
		}
		if (sp_get_lost.getSelectedItem().toString().equals("我丢掉了东西")) {
			new AlertDialog.Builder(getActivity())
					.setTitle("提示")
					.setMessage("你确定要发布“我丢掉了东西”吗？")
					.setPositiveButton("确定",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method
									// stub
									FoundFragment.initData(getActivity());
									Lost lost = new Lost();
									lost.setDescribe(miaoshu);
									lost.setPhone(phone);
									lost.setTitle(title);
									lost.save(getActivity(),
											new SaveListener() {

												@Override
												public void onSuccess() {
													// 其他代码
													DiyToast.showToast(
															getActivity(),
															"发布成功");
													FoundFragment
															.get_find_list_from_bmobsql(getActivity());
													FindFragment
															.get_find_list_from_bmobsql(getActivity());
													et_miaoshu_find
															.setText(null);
													et_phone_find.setText(null);
													et_title_find.setText(null);
												}

												@Override
												public void onFailure(int code,
														String arg0) {
													DiyToast.showToast(
															getActivity(),
															"失败：" + arg0);
												}
											});
								}
							})
					.setNegativeButton("取消",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method
									// stub

								}
							}).show();

		}
	}

	private void show_dialog_for_repass() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		final View view = LayoutInflater.from(getActivity()).inflate(
				R.layout.dialog_show_yanzheng_pass, null, false);
		builder.setView(view);
		((ImageView) view.findViewById(R.id.iv_repass_show))
				.setImageBitmap(CodeView.createRandomBitMap());
		((TextView) view.findViewById(R.id.tv_dialog_update_repass))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						CodeView.createRandomBitMap();
					}
				});
		builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if (((EditText) view.findViewById(R.id.et_dialog_repass_edit))
						.getText().toString().isEmpty()) {
					DiyToast.showToast(getActivity(), "请输入验证码");
					show_dialog_for_repass();
				} else {
					if (((EditText) view
							.findViewById(R.id.et_dialog_repass_edit))
							.getText().toString().equals(CodeView.code)) {
						con_for_find_lost();
					} else {
						DiyToast.showToast(getActivity(), "验证码错误");
						show_dialog_for_repass();
					}
				}
			}
		});
		builder.setNegativeButton("取消", null);
		builder.show();
	}
}