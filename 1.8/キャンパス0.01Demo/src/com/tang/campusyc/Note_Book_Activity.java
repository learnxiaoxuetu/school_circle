package com.tang.campusyc;

import com.tang.campusyc.mysql.MyDataBaseHelper;
import com.tang.campusyc.toast.DiyToast;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Note_Book_Activity extends Activity {
	private Button btn_note_con, btn_note_resert;
	private EditText et_notw_get;
	private MyDataBaseHelper dbHelper;
	private SQLiteDatabase db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note__book);
		initView();
		get_sql();
		btn_note_con.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (et_notw_get.getText().toString().isEmpty()) {
					DiyToast.showToast(getApplicationContext(),
							"你还没有记录什么东西......");
				} else {
					db.execSQL("update note set note = ?",
							new String[] { et_notw_get.getText().toString() });
					DiyToast.showToast(getApplicationContext(), "记录成功");
				}
			}
		});
		btn_note_resert.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				et_notw_get.setText("");
				db.execSQL("update note set note = ?", new String[] { "" });
				DiyToast.showToast(getApplicationContext(), "重置成功");
			}
		});
	}

	private void get_sql() {
		// TODO Auto-generated method stub
		Cursor cursor = db.rawQuery("select * from note", null);
		if (cursor.getCount() == 0) {
			DiyToast.showToast(getApplicationContext(), "记录点什么吧");
		} else {
			for (int i = 0; i < cursor.getCount(); i++) {
				cursor.moveToFirst();
				et_notw_get.setText(cursor.getString(cursor
						.getColumnIndex("note")));
			}
		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		dbHelper = new MyDataBaseHelper(getApplicationContext(), "info.db",
				null, 2);
		db = dbHelper.getWritableDatabase();
		btn_note_con = (Button) findViewById(R.id.btn_note_con);
		et_notw_get = (EditText) findViewById(R.id.et_note_get);
		btn_note_resert = (Button) findViewById(R.id.btn_note_resert);
	}
}