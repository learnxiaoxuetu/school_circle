package com.tang.campusyc.adapter;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * @author Administrator
 * @year 2019
 * @Todo TODO 获取当前年月日和时间
 * @package_name com.tang.campusyc.adapter
 * @project_name キャンパス0.01Demo
 * @file_name DataString.java
 */
public class DataString {
	private static String mYear;
	private static String mMonth;
	private static String mDay;
	private static String mWay;

	public static String StringData_now_week() {
		final Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
		mYear = String.valueOf(c.get(Calendar.YEAR));// 获取当前年份
		mMonth = String.valueOf(c.get(Calendar.MONTH) + 1);// 获取当前月份
		mDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH));// 获取当前月份的日期号码
		mWay = String.valueOf(c.get(Calendar.DAY_OF_WEEK));
		return mWay;
	}

	public static String StringData_now_day() {
		final Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
		mYear = String.valueOf(c.get(Calendar.YEAR));// 获取当前年份
		mMonth = String.valueOf(c.get(Calendar.MONTH) + 1);// 获取当前月份
		mDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH));// 获取当前月份的日期号码
		return mYear + "年" + mMonth + "月" + mDay + "日";
	}
}
